# Traverse Extension

- It Provides the source code for chrome extension created for the purpose of 
  Navigate to all the Technia URLs in one place, Manage your JIRA tickets, Monitor & export the time spent on the browser.
    1. [UI_1](https://gitlab.com/pgupta1795/Traverse-Technia/-/blob/master/Working/UI_1.PNG)
    2. [UI_2](https://gitlab.com/pgupta1795/Traverse-Technia/-/blob/master/Working/UI_2.PNG)
    3. [UI_3](https://gitlab.com/pgupta1795/Traverse-Technia/-/blob/master/Working/UI_3.PNG)
    4. [UI_4](https://gitlab.com/pgupta1795/Traverse-Technia/-/blob/master/Working/UI_4.PNG)
    5. [UI_5](https://gitlab.com/pgupta1795/Traverse-Technia/-/blob/master/Working/UI_4.1.PNG)
    
<BR>

- Add Extension to Chrome :<BR>
    https://chrome.google.com/webstore/detail/traverse-technia/ndlngenadkabhgedonbbnamimfpnjedi/related?authuser=0&hl=en-GB

- Add Extension to firefox :<BR>
    https://addons.mozilla.org/en-US/firefox/addon/traverse-technia/

- STEPS to add extension manually :
    1.	Clone the respository on your local in a folder
    2.	Go to chrome://extensions/ in your chrome browser and make sure Developer Mode is switched on
    3.	Click on Load unpacked button and select folder where the files are placed.
